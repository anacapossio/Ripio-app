import React from 'react'
import './MainSection.css'
import Aside from '../Aside/Aside'
import Form from '../Form/Form'

const MainSection = ({...props}) =>
  <section className='MainSection'>
    <Aside />
    <Form />
  </section>

export default MainSection
