import React, { Component } from 'react'
import './Aside.css'
import logo from '../../../images/logo_ripio.svg';
import avatar from '../../../images/avatar_ricar.png';

class Aside extends Component {

  render () {
    return(
      <aside className='Aside'>
        <div className='imageAside'>
          <img  src={logo} alt="Ripio" />
        </div>
        <div className='imageAside'>
          <img src={avatar} alt="Ricardo" />
        </div>
        <h3>Matias Albaneco</h3>
        <p>BTC: 0.05</p>
      </aside>
    )
  }
}

export default Aside
