import React from 'react'
import { Field, reduxForm } from 'redux-form'
import './SyncValidationForm.css'

const required = value => (value ? undefined : 'Requerido!')
const maxLength = max => value =>
  value && value.length > max ? `Debe ser ${max} caracteres o menos` : undefined
const maxLength35 = maxLength(35)
export const minLength = min => value =>
  value && value.length < min ? `Debe ser ${min} caracteres o más` : undefined
export const minLength26 = minLength(26)
const toobtc = value =>
  value && value > 0.05 ? 'No tenes suficientes BTC' : undefined
const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Solamente caracteres Alfanumericos'
    : undefined
export const phoneNumber = value =>
  value && !/^(0|[1-9][0-9]{9})$/i.test(value)
    ? 'Invalid phone number, must be 10 digits'
    : undefined

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div className="ValidationForm">
    <div className="ValidationFormLabel">
      <label>{label}
        <input {...input}  type={type} />
      </label>
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
)

const FieldLevelValidationForm = props => {
  const { handleSubmit, submitting } = props
  return (
    <form onSubmit={handleSubmit}>
      <Field
        name="address"
        type="text"
        component={renderField}
        label="Dirección de Bitcoins"
        validate={[required, maxLength35, minLength26, alphaNumeric ]}
        warn={alphaNumeric}
      />

      <Field
        name="bitcoins"
        type="number"
        component={renderField}
        label="Cantidad de Bitcoins"
        validate={[required,toobtc]}
      />
      <div className="ValidationForm">
        <button type="submit" disabled={submitting}>
          Enviar
        </button>
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'fieldLevelValidation' // a unique identifier for this form
})(FieldLevelValidationForm)