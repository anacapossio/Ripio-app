const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export default (async function showResults(values) {
  await sleep(1500); // simulate server latency
  if (['0.05', '0.005'].includes(values.bitcoins)) {
    // eslint-disable-next-line no-throw-literal
    throw { bitcoins: 'Bitcoins' }
  }
 ;
});
