import React, { Component } from 'react'

import MainSection from './components/general/MainSection/MainSection'

class App extends Component {
  render () {
    return (
      <main>
        <MainSection/>
      </main>
    )
  }
}

export default App
