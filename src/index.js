import React from "react";
import ReactDOM from "react-dom";
import './styles/main.css'
import { Provider } from "react-redux";
import store from "./components/general/SyncValidationForm/store";
import MainSection from './components/general/MainSection/MainSection'


const rootEl = document.getElementById("root");

ReactDOM.render(
  <Provider store={store}>
      <MainSection/>
  </Provider>,
  rootEl
);
