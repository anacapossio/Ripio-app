# Ripio Form!
![Ripio Logo](https://i.imgur.com/sSyidDx.png "Ripio")

Ripio Form, prueba de concepto con:

  - [ReactJS](https://reactjs.org/)
  - [Redux Form](https://redux-form.com)
  - Magic (html + css 💅)

# Go for it 🚀

  - Run 'npm intall' (Only for the first time)
  - Then 'npm start'
  - And you are all set

# Wireframe

![Wireframe](https://i.imgur.com/R0feSEg.png)
